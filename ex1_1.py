import numpy as np
import pandas as pd
from Perceptron import *

# read in the dataset
spamX = pd.read_csv("spambase/spambase_X.csv", header=None).to_numpy()
spamX = spamX.T
spamY = pd.read_csv("spambase/spambase_y.csv", header=None).to_numpy()

#create a 0s filled w vector & bias
w = np.zeros((1,spamX.shape[1]))
b = 0

# initialize a Perceptron (documentation in Perceptron.py)
spamPerceptron = Perceptron(spamX, spamY, w, b, 500)

# train the perceptron; graphes errors per pass upon completion
spamPerceptron.train(errorPlot=True)