### Referenced this webpage to aid in row removal during the process of training all one vs one perceptrons
# https://www.geeksforgeeks.org/how-to-remove-rows-from-a-numpy-array-based-on-multiple-conditions/

from re import A
import numpy as np
import pandas as pd
from Perceptron import *
from ReadData import *

# read in the training dataset
activityTrainingX = ReadX("activityData/activity_X_train.txt")
activityTrainingY = ReadY("activityData/activity_y_train.txt")
# read in the test dataset
activityTestingX = ReadX("activityData/activity_X_test.txt")
activityTestingY = ReadY("activityData/activity_y_test.txt")

# given each class, create a one vs. all other perceptron
allClasses = np.unique(activityTrainingY)
classPerceptrons = {}

#create k - 0s filled w vector & bias
w = np.zeros((allClasses.shape[0],activityTrainingX.shape[1]))
b = np.ones((allClasses.shape[0], 1))

# these will hold all predictions for final classification
trainingPredictions = np.zeros((activityTrainingX.shape[0], allClasses.shape[0]))
testingPredictions = np.zeros((activityTestingX.shape[0], allClasses.shape[0]))

# initialize a Perceptron (documentation in Perceptron.py)
activityPerceptron = Perceptron(activityTrainingX, activityTrainingY, w, b, 500)

# train the perceptron
activityPerceptron.trainMultipleW(errorPlot=False)

# predict all in the training set
trainPrediction = activityPerceptron.predictPointsMultipleW(activityTrainingX)
# predict all in the test set
testPrediction = activityPerceptron.predictPointsMultipleW(activityTestingX)

# # place predictions in corresponding columns of the prediction matrix
# trainingPredictions = trainPrediction
# testingPredictions = testPrediction

# finally, get error rate of these perceptrons by deciding class membership
mistakes = 0
for i in range(trainPrediction.shape[0]):
    # grab highest score prediction
    yHat = trainPrediction[i]
    yTrue = activityTrainingY[i]

    # update error
    if yHat != int(yTrue):
        mistakes += 1
print("\n--- Error on training set ---")
print(f"Total Errors: {mistakes}")
print(f"Error rate: {mistakes / activityTrainingY.shape[0]}")

mistakes = 0
for i in range(testPrediction.shape[0]):
    # grab highest score prediction
    yHat = testPrediction[i]
    yTrue = activityTestingY[i]

    # update error
    if yHat != int(yTrue):
        mistakes += 1
print("\n--- Error on testing set ---")
print(f"Total Errors: {mistakes}")
print(f"Error rate: {mistakes / activityTestingY.shape[0]}")