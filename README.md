Callum Takasaka

21000024

# Spamceptron

## Requirements

- Python 3.6 + (atleast I think newer versions are fine, but 3.6 was suggested by prof)

- numpy, pandas, matplotlib, ReadData.py, Perceptron.py (last 2 are provided in zip)

## Running Code

Only the files denoted "ex1_*.py" must be run. Script names correspond to the subquestion.

