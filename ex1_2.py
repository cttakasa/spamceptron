from re import A
import numpy as np
import pandas as pd
from Perceptron import *
from ReadData import *

# read in the training dataset
activityTrainingX = ReadX("activityData/activity_X_train.txt")
activityTrainingY = ReadY("activityData/activity_y_train.txt")
# read in the test dataset
activityTestingX = ReadX("activityData/activity_X_test.txt")
activityTestingY = ReadY("activityData/activity_y_test.txt")

#create a 0s filled w vector & bias
w = np.zeros((1,activityTrainingX.shape[1]))
b = 0

# given each class, create a one vs. all other perceptron
allClasses = np.unique(activityTrainingY)
classPerceptrons = {}

# these will hold all predictions for final classification
trainingPredictions = np.zeros((activityTrainingX.shape[0], allClasses.shape[0]))
testingPredictions = np.zeros((activityTestingX.shape[0], allClasses.shape[0]))

for i, y in enumerate(allClasses):
    # change y to properly represent one vs all other
    alteredTrainingY = np.copy(activityTrainingY)
    alteredTrainingY[alteredTrainingY != y] = -1
    alteredTrainingY[alteredTrainingY != -1] = 1
    # do the same for the test set
    alteredTestY = np.copy(activityTestingY)
    alteredTestY[alteredTestY!= y] = -1
    alteredTestY[alteredTestY!= -1] = 1

    # initialize a Perceptron (documentation in Perceptron.py)
    activityPerceptron = Perceptron(activityTrainingX, alteredTrainingY, w, b, 500)

    # train the perceptron
    activityPerceptron.train(errorPlot=False)

    # predict all in the training set
    trainPrediction = activityPerceptron.predictPoints(activityTrainingX)
    # predict all in the test set
    testPrediction = activityPerceptron.predictPoints(activityTestingX)

    # place predictions in corresponding columns of the prediction matrix
    trainingPredictions[:,i] = trainPrediction[:,0]
    testingPredictions[:,i] = testPrediction[:,0]

# finally, get error rate of these perceptrons by deciding class membership
mistakes = 0
for i in range(trainingPredictions.shape[0]):
    # grab highest score prediction
    yHat = allClasses[np.argmax(trainingPredictions[i])]
    yTrue = activityTrainingY[i]

    # update error
    if yHat != yTrue:
        mistakes += 1
print("\n--- Error on test set ---")
print(f"Total Errors: {mistakes}")
print(f"Error rate: {mistakes / activityTrainingY.shape[0]}")

mistakes = 0
for i in range(testingPredictions.shape[0]):
    # grab highest score prediction
    yHat = allClasses[np.argmax(testingPredictions[i])]
    yTrue = activityTestingY[i]

    # update error
    if yHat != yTrue:
        mistakes += 1
print("\n--- Error on test set ---")
print(f"Total Errors: {mistakes}")
print(f"Error rate: {mistakes / activityTestingY.shape[0]}")