### Referenced this webpage to aid in row removal during the process of training all one vs one perceptrons
# https://www.geeksforgeeks.org/how-to-remove-rows-from-a-numpy-array-based-on-multiple-conditions/

from re import A
import numpy as np
import pandas as pd
from Perceptron import *
from ReadData import *

# read in the training dataset
activityTrainingX = ReadX("activityData/activity_X_train.txt")
activityTrainingY = ReadY("activityData/activity_y_train.txt")
# read in the test dataset
activityTestingX = ReadX("activityData/activity_X_test.txt")
activityTestingY = ReadY("activityData/activity_y_test.txt")

alteredTrainingY = np.append(activityTrainingX.T, [activityTrainingY], axis=0).T
cleaned = np.delete(alteredTrainingY, np.where((alteredTrainingY[:, -1] != 1) & (alteredTrainingY[:, -1] != 6))[0], axis=0)

#create a 0s filled w vector & bias
w = np.zeros((1,activityTrainingX.shape[1]))
b = 0

# given each class, create a one vs. all other perceptron
allClasses = np.unique(activityTrainingY)
classPerceptrons = {}

# these will hold all predictions for final classification (15 columns for each perceptron, 6 choose 2, votes)
trainingPredictions = np.zeros((activityTrainingX.shape[0], 15))
testingPredictions = np.zeros((activityTestingX.shape[0], 15))

perceptronCounter = 0
for i, y in enumerate(allClasses):
    for j, y in enumerate(allClasses):
        # ignore cases of i == j, and only compute n choose 2 perceptrons
        if j <= i: continue

        # get proper class names
        iClass = allClasses[i]
        jClass = allClasses[j]

        # shape y to only contain classes i & j (i = 1)
        alteredTraining = np.append(activityTrainingX.T, [activityTrainingY], axis=0).T
        alteredTraining = np.delete(alteredTraining, np.where((alteredTraining[:, -1] != iClass) & (alteredTraining[:, -1] != jClass))[0], axis=0)
        alteredTrainingX = alteredTraining[:,:-1]
        alteredTrainingY = alteredTraining[:,-1]
        alteredTrainingY[alteredTrainingY == iClass] = 1   
        alteredTrainingY[alteredTrainingY == jClass] = -1

        # # do the same for the test set
        alteredTesting = np.append(activityTestingX.T, [activityTestingY], axis=0).T
        alteredTesting = np.delete(alteredTesting, np.where((alteredTesting[:, -1] != iClass) & (alteredTesting[:, -1] != jClass))[0], axis=0)
        alteredTestingX = alteredTesting[:,:-2]
        alteredTestingY = alteredTesting[:,-1]
        alteredTestingY[alteredTestingY == iClass] = 1   
        alteredTrainingY[alteredTrainingY == jClass] = -1

        # # initialize a Perceptron (documentation in Perceptron.py)
        activityPerceptron = Perceptron(alteredTrainingX, alteredTrainingY, w, b, 500)

        # # train the perceptron
        activityPerceptron.train(errorPlot=False)

        # # predict all in the training set
        trainPrediction = activityPerceptron.predictPointVotes(activityTrainingX)
        trainPrediction[trainPrediction == 1.0] = iClass
        trainPrediction[trainPrediction == -1.0] = jClass
        # # predict all in the test set
        testPrediction = activityPerceptron.predictPointVotes(activityTestingX)
        testPrediction[testPrediction == 1.0] = iClass
        testPrediction[testPrediction == -1.0] = jClass

        # place predictions in corresponding columns of the prediction matrix
        trainingPredictions[:,perceptronCounter] += trainPrediction[:,0]
        testingPredictions[:,perceptronCounter] += testPrediction[:,0]

        perceptronCounter += 1

# finally, get error rate of these perceptrons by tallying votes
mistakes = 0
for i in range(trainingPredictions.shape[0]):
    # grab highest score prediction
    x = trainingPredictions[i].astype(int)
    yHat = np.bincount(x).argmax()
    yTrue = activityTrainingY[i].astype(int)

    # update error
    if yHat != yTrue:
        mistakes += 1
print("\n--- Error on training set ---")
print(f"Total Errors: {mistakes}")
print(f"Error rate: {mistakes / activityTrainingY.shape[0]}")

mistakes = 0
for i in range(testingPredictions.shape[0]):
    # grab highest score prediction
    x = testingPredictions[i].astype(int)
    yHat = np.bincount(x).argmax()
    yTrue = activityTestingY[i].astype(int)

    # update error
    if yHat != yTrue:
        mistakes += 1
print("\n--- Error on test set ---")
print(f"Total Errors: {mistakes}")
print(f"Error rate: {mistakes / activityTestingY.shape[0]}")