import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class Perceptron:
    """
    A perceptron; classifier class
    """

    # initialize a Perceptron with given parameters & a default max passes of 500
    def __init__(self, x, y, w, b, maxPass=500):
        self.x = x
        self.y = y
        self.w = w
        self.b = b
        self.maxPass = maxPass

    # predict the (binary) class membership of a given an index of x
    def predict(self, idx):
        # calculate yHat, and then threshold against 0 (<=0 assigns class membership of 0, otherwise 1)
        return np.sign(np.dot(self.w, self.x[idx]) + self.b)

    # return all predictions w.r.t. wk
    def predictMultipleW(self, idx):
        # calculate yHat w.r.t. all wk
        predictions = np.zeros((self.w.shape[0],1))
        for i, wk in enumerate(self.w):
            predictions[i] = np.dot(wk, self.x[idx]) + self.b[i]
        return predictions

    # iteratively predict the (binary) class membership of each datapoint in X, and account for mistakes
    # this will run for [1, maxPass] iterations, each of length |X|
    def train(self, errorPlot=False):
        print(f"Beginning training! Max passes: {self.maxPass}")

        # keep track of number of mistakes across all passes (iterations)
        mistakesPerPass = [];
        # iterate thru a "step" of training
        for t in range(self.maxPass):
            print(f"Pass: {t+1}/{self.maxPass}")
            # track if the model has already converged
            anyMistakes = False
            mistakeCounter = 0
            # iterate thru X every pass
            for i in range(self.x.shape[0]):
                mistake = False
                yHat = self.predict(i)
                yTrue = self.y[i]

                #check for mistake in class membership prediction
                #if -1, point is on boundary, act hostile.
                if yHat == 0.0:
                    mistake = True
                elif yHat != yTrue:
                    mistake = True
                # print(f"Calculated mistake: {mistake}")
                if mistake:
                    # update our anyMistakes var
                    anyMistakes = True
                    mistakeCounter += 1

                    # add x to w (yTrue allows for proper "direction" of the error)
                    self.w += (yTrue * self.x[i])
                    # add yTrue to the bias (same case as above, but the bias always multiplies with 1, since we add it)
                    self.b += yTrue

            # check for convergence, break if done
            if not anyMistakes:
                mistakesPerPass.append(0)
                break

            # print(f"Completed Pass: {t}, with {mistakeCounter} mistakes")
            mistakesPerPass.append(mistakeCounter)

        if errorPlot:
            # output the plot of error over pass count
            # plt.style.use('_mpl-gallery')
            fig, ax = plt.subplots()

            passes = np.arange(len(mistakesPerPass))
            ax.plot(passes, mistakesPerPass)
            ax.set(xticks=np.arange(0,self.maxPass+1,self.maxPass/10))
            ax.set_ylabel('Errors')
            ax.set_xlabel('Passes')

            plt.show()
    
    def trainMultipleW(self, errorPlot=False):
        print(f"Beginning training! Max passes: {self.maxPass}")

        # keep track of number of mistakes across all passes (iterations)
        mistakesPerPass = []
        # iterate thru a "step" of training
        for t in range(self.maxPass):
            print(f"Pass: {t+1}/{self.maxPass}")
            # track if the model has already converged
            anyMistakes = False
            mistakeCounter = 0
            # iterate thru X every pass
            for i in range(self.x.shape[0]):
                mistake = False
                yTrue = self.y[i]
                yHat = self.predictMultipleW(i)
                #check for mistake in class membership prediction
                classPrediction = np.argmax(yHat) + 1
                if classPrediction != int(yTrue):
                    mistake = True
                # add this x to the corresponding (correct) wk
                if mistake:
                    # update our anyMistakes var
                    anyMistakes = True
                    mistakeCounter += 1
                    # add x to w (yTrue allows for proper "direction" of the error)
                    properClassIdx = int(yTrue)-1
                    self.w[properClassIdx] += self.x[i]
                    # add yTrue to the bias (same case as above, but the bias always multiplies with 1, since we add it)
                    self.b[properClassIdx] += 1

            # check for convergence, break if done
            if not anyMistakes:
                mistakesPerPass.append(0)
                break

            # print(f"Completed Pass: {t}, with {mistakeCounter} mistakes")
            mistakesPerPass.append(mistakeCounter)

        if errorPlot:
            # output the plot of error over pass count
            # plt.style.use('_mpl-gallery')
            fig, ax = plt.subplots()

            passes = np.arange(len(mistakesPerPass))
            ax.plot(passes, mistakesPerPass)
            ax.set(xticks=np.arange(0,self.maxPass+1,self.maxPass/10))
            ax.set_ylabel('Errors')
            ax.set_xlabel('Passes')

            plt.show()

    def predictPoints(self, x):
        predictions = np.zeros((x.shape[0], 1))
        for i in range(x.shape[0]):
            yHat = np.dot(self.w, x[i]) + self.b
            predictions[i] = yHat
        return predictions
    
    def predictPointVotes(self, x):
        predictions = np.zeros((x.shape[0], 1))
        for i in range(x.shape[0]):
            yHat = np.sign(np.dot(self.w, x[i]) + self.b)
            predictions[i] = yHat
        return predictions

    # TODO
    def predictPointsMultipleW(self, x):
        predictions = np.zeros((x.shape[0], 1))
        for i in range(x.shape[0]):
            allPredictions = np.zeros((self.w.shape[0],1))
            for j, wk in enumerate(self.w):
                allPredictions[j] = np.dot(wk, x[i]) + self.b[j]
            bestPrediction = np.argmax(allPredictions) + 1
            predictions[i] = bestPrediction
        print(predictions.shape)
        return predictions

    def resetParams(self):
        self.w = np.zeros((self.w.shape[0], self.w.shape[1]))
        self.b = 0